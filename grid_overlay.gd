tool
extends Control

export var grid_x: float
export var grid_y: float
export var grid_color: Color

func _draw() -> void:
	var step_x := get_rect().size.x / grid_x
	var step_y := get_rect().size.y / grid_y
	for x in range(0, grid_x - 1):
		var pos := x * step_x + step_x
		draw_line(Vector2(pos, 0.0), Vector2(pos, get_rect().size.y), grid_color)
	for y in range(0, grid_y - 1):
		var pos := y * step_y + step_y
		draw_line(Vector2(0.0, pos), Vector2(get_rect().size.x, pos), grid_color)
