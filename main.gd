extends Control


export var zoom_step: float = 1.0
export var tool_options_popup_size: Vector2 = Vector2(200.0, 200.0)


# the gameboy graphics scene
var project: CGBScene = CGBScene.new()
var project_changed: bool = false

# position offsets of the view
var view_pos: Vector2 = Vector2.ZERO
var view_scale: float = 1.0

# which palette color to paint
var selected_color: int = 0

var selected_tool: int = 0
var tools := [
	preload("res://tools/pencil.gd").new()
]


onready var editor: Control = $VBoxContainer/HBoxContainer/Editor
onready var view: TextureRect = $VBoxContainer/HBoxContainer/Editor/SceneView
onready var zoom_level: LineEdit = $VBoxContainer/HBoxContainer/Editor/Zoom/ZoomLevel
onready var tool_options_popup: PopupDialog = $ToolOptions
onready var tool_options: VBoxContainer = $ToolOptions/MarginContainer/Options

onready var color_buttons := [
	$VBoxContainer/HBoxContainer/RightPanel/Palettes/Selector/Color0,
	$VBoxContainer/HBoxContainer/RightPanel/Palettes/Selector/Color1,
	$VBoxContainer/HBoxContainer/RightPanel/Palettes/Selector/Color2,
	$VBoxContainer/HBoxContainer/RightPanel/Palettes/Selector/Color3
]


func _ready() -> void:
	view.texture = ImageTexture.new()
	view.texture.create_from_image(project.rendered_image, 0)


func _process(delta: float) -> void:
	if project_changed:
		_update_view()
		project_changed = false


func _update_view() -> void:
	project.update_image()
	view.texture.set_data(project.rendered_image)


func select_color(color: int) -> void:
	selected_color = color
	color_buttons[color].pressed = true


func _resize() -> void:
	var center := editor.rect_size / 2.0
	view.rect_size = Vector2(CGBScene.CANVAS_WIDTH, CGBScene.CANVAS_HEIGHT) * view_scale
	view.rect_position = center - view.rect_size / 2.0 + view_pos


func _input(event: InputEvent) -> void:
	var kb_event := event as InputEventKey
	if kb_event and kb_event.pressed:
		match kb_event.scancode:
			KEY_1:
				select_color(0)
			KEY_2:
				select_color(1)
			KEY_3:
				select_color(2)
			KEY_4:
				select_color(3)
			_:
				pass

func _on_editor_resized() -> void:
	_resize()


func _on_palette_color_changed(palette: int, idx: int, color: Color):
	project.palettes[palette][idx] = color
	project_changed = true


func _on_zoom_out_pressed() -> void:
	view_scale = clamp(view_scale - zoom_step, 1.0, 10.0)
	zoom_level.text = "%d" % (view_scale * 100.0)
	_resize()


func _on_zoom_in_pressed() -> void:
	view_scale = clamp(view_scale + zoom_step, 1.0, 10.0)
	zoom_level.text = "%d" % (view_scale * 100.0)
	_resize()


func _on_zoom_level_text_changed(new_text: String) -> void:
	if new_text.is_valid_integer():
		view_scale = new_text.to_float() / 100.0
		_resize()


func _on_editor_gui_input(event: InputEvent) -> void:
	var mb_event := event as InputEventMouseButton
	if mb_event:
		if mb_event.button_index == BUTTON_WHEEL_DOWN:
			view_scale = clamp(view_scale - zoom_step, 1.0, 32.0)
		elif mb_event.button_index == BUTTON_WHEEL_UP:
			view_scale = clamp(view_scale + zoom_step, 1.0, 32.0)
		zoom_level.text = "%d" % (view_scale * 100.0)
		_resize()
	
	var mm_event := event as InputEventMouseMotion
	if mm_event and Input.is_mouse_button_pressed(BUTTON_MIDDLE):
		view_pos += mm_event.relative
		_resize()


func _on_view_gui_input(event: InputEvent) -> void:
#	var mb_event := event as InputEventMouseButton
#	if mb_event and mb_event.button_index == BUTTON_LEFT and mb_event.pressed:
	var m_event := event as InputEventMouse
	if m_event and m_event.button_mask & BUTTON_MASK_LEFT != 0:
		var selected_pixel := (m_event.position / view_scale)
		var x := int(selected_pixel.x)
		var y := int(selected_pixel.y)
		if tools[selected_tool]:
			if tools[selected_tool].click_tick(project, x, y, selected_color):
				project_changed = true


func _on_tool_selected(index: int) -> void:
	pass # Replace with function body.


func _on_tool_options(index: int, at_position: Vector2) -> void:
	for child in tool_options.get_children():
		tool_options.remove_child(child)
		child.queue_free()
	
	tools[index].config(tool_options)
	tool_options_popup.popup(Rect2(at_position, tool_options_popup_size))
