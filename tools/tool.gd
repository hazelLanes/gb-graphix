class_name Tool
extends Node

func click_down(scene: CGBScene, x: int, y: int, color: int) -> bool:
	return false

func click_up(scene: CGBScene, x: int, y: int, color: int) -> bool:
	return false

func click_tick(scene: CGBScene, x: int, y: int, color: int) -> bool:
	return false

func config(list: VBoxContainer) -> void:
	pass

func config_option(title: String, option: Control) -> HBoxContainer:
	var hbox := HBoxContainer.new()
	var label := Label.new()
	label.text = title
	hbox.add_child(label)
	hbox.add_child(option)
	return hbox
