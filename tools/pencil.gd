extends Tool

var size: int = 1

func click_tick(scene: CGBScene, x: int, y: int, color: int) -> bool:
	for xx in range(x - size / 2, x + size / 2 + 1):
		for yy in range(y - size / 2, y + size / 2 + 1):
			scene.set_pixel(xx, yy, color)
	return true

func config(list: VBoxContainer) -> void:
	var size_spinbox := SpinBox.new()
	size_spinbox.value = size
	size_spinbox.min_value = 1.0
	size_spinbox.max_value = 16.0
	size_spinbox.step = 1.0
	size_spinbox.connect("value_changed", self, "_on_size_changed")
	list.add_child(config_option("size", size_spinbox))

func _on_size_changed(value: float) -> void:
	size = int(value)
