class_name CGBScene
extends Resource

class Tile:
	const WIDTH: int = 8
	const HEIGHT: int = 8
	
	var palette: int = 0
	var pixels: Array = []
	
	func _init() -> void:
		pixels.resize(WIDTH * HEIGHT)
		for i in range(WIDTH * HEIGHT):
			pixels[i] = 0
	
	func get_pixel(x: int, y: int) -> int:
		return pixels[x + y * WIDTH]
	
	func set_pixel(x: int, y: int, value: int) -> void:
		if x < 8 and y < 8 and value < 4:
			pixels[x + y * WIDTH] = value


const TILEMAP_WIDTH: int = 20
const TILEMAP_HEIGHT: int = 18
const CANVAS_WIDTH: int = 160
const CANVAS_HEIGHT: int = 144
const MAX_PALETTES: int = 8


var palettes: Array = []
var tiles: Array
var rendered_image: Image


func _init() -> void:
	# initialize palettes
	palettes.resize(MAX_PALETTES)
	for i in range(MAX_PALETTES):
		palettes[i] = [Color.black, Color.black, Color.green, Color.black]
	
	# initialize tiles
	tiles.resize(TILEMAP_WIDTH * TILEMAP_HEIGHT)
	for i in range(TILEMAP_WIDTH * TILEMAP_HEIGHT):
		tiles[i] = Tile.new()
	
	# set up image
	rendered_image = Image.new()
	rendered_image.create(CANVAS_WIDTH, CANVAS_HEIGHT, false, Image.FORMAT_RGB8)
	update_image()


func get_tile(x: int, y: int) -> Tile:
	return tiles[x + y * TILEMAP_WIDTH]


func set_pixel(x: int, y: int, color: int):
	var tile := get_tile(x / Tile.WIDTH, y / Tile.HEIGHT)
	tile.set_pixel(x % Tile.WIDTH, y % Tile.HEIGHT, color)


func update_image() -> void:
	rendered_image.lock()
	
	for x in range(TILEMAP_WIDTH):
		for y in range(TILEMAP_HEIGHT):
			var tile := get_tile(x, y)
			for xx in range(Tile.WIDTH):
				for yy in range (Tile.HEIGHT):
					var pixel := tile.get_pixel(xx, yy)
					var color: Color = palettes[tile.palette][pixel]
					rendered_image.set_pixel(
						x * Tile.WIDTH + xx,
						y * Tile.HEIGHT + yy,
						color
					)
	
	rendered_image.unlock()
