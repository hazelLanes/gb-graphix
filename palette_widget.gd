extends HBoxContainer

signal color_changed(palette, idx, color)

export var palette: int

onready var colors := [
	$Color0,
	$Color1,
	$Color2,
	$Color3
]

func _ready() -> void:
	$Label.text = "%d:" % palette

func _on_color_changed(color: Color, idx: int):
	color.r = floor(color.r * 32.0)/32.0
	color.g = floor(color.g * 32.0)/32.0
	color.b = floor(color.b * 32.0)/32.0
	color.a = 1.0
	colors[idx].color = color
	emit_signal("color_changed", palette, idx, color)
